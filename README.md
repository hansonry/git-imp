# git-imp

A bash robot that helps me with some of my more repetitive git tasks

## Help Dump

```
$ git-imp help
git-imp - A helpful tool to supplement git

Warning: This tool will not protect you from making mistakes. There
         is no checking built into this tool to make sure everything
         will come out right. You should understand git and git submodules
         before using this tool. All it does is cut down on some of the
         repetitive effort.

git-imp [ newbranch | status | checkout | attach | repin | push | deletebranch |
          home | check | save | load | pull ]

newbranch | nb    - Creates new branches in each submodule from the currently
                    checked out branches. The name of the branch is the first
                    parameter. You can use -i after the branch name to
                    only create branches in specific submodules.
status            - A short status of all of the submodules
checkout | co     - Checkout the branch and attempt to attach it
attach            - Attempt to attach all Detached Heads to the branch
                    of this repository. Once checked out a pull is called
                    to update if there is a remote branch. By default
                    this will not change modules that have a branch. Use
                    -f to change all the branches.
repin             - Makes commits to all branches so that all submodules
                    are up to date. You will want to push afterword.
                    This will NOT work if the submodule is already added
                    to the index. By default the commit messages will
                    generate automatically and they will be somewhat verbose.
                    You can specify -m and give a commit message.
                    You can also specify -lcm to reuse the last commit
                    message on the lowest offending submodule.
push              - Pushes all branches in the submodule tree. If you
                    use the -u flag then you must specify a remote (origin)
                    and push will push all local branches that don't have
                    a remote up to the specified remote.
deletebranch | db - Removes all local branches in each submodule with the
                    Name provided by the first parameter. Adding -f after
                    the first parameter will force delete the local branch.
                    Adding a -u after and a remote name (origin) will result
                    in this command recursively deleting all branches from
                    the remote specified.
home              - Prints out the root directory of the submodule tree. You
                    can go there by "cd `git-imp home`"
check             - Check if submodules are behind or ahead. This is useful
                    if people have not been pinning well.
save              - Saves all the branches and Heads of the currently
                    checked out branch of the root repository. This is
                    saved in a file named .git-imp-saves.
load              - Looks at the current root branch and tries to find
                    a matching record in .git-imp-saves.
                    If it does, it will set all the submodule branches
                    to the records in that file.
pull              - Does a "git pull" in every submodule.
```

## Installing

Just checkout the repo and add `./bin/git-imp` to your path or call it directly.
