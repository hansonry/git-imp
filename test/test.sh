#!/bin/bash

pushd . > /dev/null

cd `dirname "$0"`

source bash_test_tools/bash_test_tools

export PATH=$PATH:`pwd`/../bin/


# Uility functions

#####################
# util_git_create path_to_repo [submodule_to_add1 submodule_to_add2 ....]
#
# Creates a git repository at the specific location and adds submodule
# paths. All arguments are realitive to the CWD
function util_git_create
{
   local gpath=$1
   local lpath=`pwd`
   shift
   pushd . > /dev/null
   mkdir -p $gpath
   cd $gpath
   git init
   echo "A Repository at path `pwd`/$gpath" > decription.md

   while (( "$#" )); do
      git submodule add $lpath/$1 > /dev/null 2>&1
      shift
   done

   git add . > /dev/null 2>&1
   git commit -m "First Commit for $gpath" > /dev/null 2>&1
   popd > /dev/null
}

#####################
# util_git_current_branch [path_to_repo]
#
# Writes the active branch of the current repository or the repository
# specified by path_to_repo. All arguments are realitive to the CWD.
function util_git_current_branch
{
   pushd . > /dev/null
   if [ ! -z $1 ]
   then
      cd $1
   fi
   echo "`git rev-parse --abbrev-ref HEAD`"
   popd > /dev/null
}

#####################
# util_git_cloneinto remote_repo
#
# Clones the specified repository and init and update all submodules.
# After this function your CWD will be inside the new repository
# All arguments are realitive to the initial CWD
function util_git_cloneinto
{
   local gitrepo=$1
   git clone $gitrepo
   cd `basename $gitrepo`
   git submodule init > /dev/null 2>&1
   git submodule update --recursive > /dev/null 2>&1
   
}

# setup / tear down
function setup
{
  mkdir -p work
  cd work
  pushd . > /dev/null
}

function teardown
{
  popd > /dev/null
  cd ..
  rm -rf work
}

# Add test functions here
function test_not_in_git_directory
{
  # Run
  run "git-imp"
  # Assert
  assert_exit_fail
  assert_output_contains "Master this is not a git repo. I cannot help you here."
}

function test_help_no_args
{
  util_git_create "remote/simple"
  util_git_cloneinto "remote/simple"
  # Run
  run "git-imp"
  # Assert
  assert_success
  assert_output_contains "Not a valid Command."
  assert_output_contains "git-imp"
}

function test_help
{
  util_git_create "remote/simple"
  util_git_cloneinto "remote/simple"
  # Run
  run "git-imp help"
  # Assert
  assert_success
  assert_output_contains "git-imp"
  assert_output_not_contains "Not a valid Command."
}

function test_new_branch
{
  util_git_create "remote/simple"
  util_git_create "remote/level2" "remote/simple" 
  util_git_cloneinto "remote/level2"
  # Run
  run "git-imp nb test"
  # Assert
  assert_exit_success

  assert_equal $(util_git_current_branch)          "test"
  assert_equal $(util_git_current_branch "simple") "test"

  #assert_output_contains "Master"
}

function test_new_branch_deep
{
  util_git_create "remote/simple1"
  util_git_create "remote/simple2"
  util_git_create "remote/level2" "remote/simple1" "remote/simple2"
  util_git_create "remote/level3" "remote/level2"
   
  util_git_cloneinto "remote/level3"
  # Run
  run "git-imp nb deep"
  # Assert
  assert_exit_success

  assert_equal $(util_git_current_branch)                  "deep"
  assert_equal $(util_git_current_branch "level2")         "deep"
  assert_equal $(util_git_current_branch "level2/simple1") "deep"
  assert_equal $(util_git_current_branch "level2/simple2") "deep"

  #assert_output_contains "Master"
}

function test_status_small
{
  util_git_create "remote/simple"
  util_git_cloneinto "remote/simple"
  # Run
  run "git-imp sm status"
  # Assert
  assert_success


  #assert_output_contains "Master"
}

# Hide the fact that we are in a git repo
mv ../.git ../.git-temp
pushd .  > /dev/null
# Run all test functions - optional argument passed to run specific tests only
testrunner

popd > /dev/null
# Unhide the git repo
mv ../.git-temp ../.git

popd > /dev/null

